var playState = {
  create: function() {
    //Defines all assets needed for the play state
    game.stage.backgroundColor = '#CEDCF0';
    game.global.score = 0;
    this.currSubState = "intro";
    this.tallMountain = new Mountain(game, 'high_mountain', -40);
    this.shortMountain = new Mountain(game, 'low_mountain', -120);
    this.score = new CustomText(game, game.world.centerX, (1/3)*game.world.height,
    "0", locFit(150, true), {fill: '#e5ebf6'});
    this.brambles = new Brambles(game, locFit(160, true));
    this.ground = new Ground(game);
    this.player = new Dandie(game);
    this.indicator = new Indicator(game, game.world.centerY);
    this.petals = new Petals(game);
    this.resetBox = new ResetBox(game, this.player, this.petals);
    this.ceiling = new HitBox(game, 0);
    this.shootInput = new ShootKey(game);
    
    var startAction;
    if(!game.device.desktop){ startAction = "TAP TO START"; }
		else{ startAction = "SPACEBAR TO START"; }
    this.startLabel = new CustomText(game, game.world.centerX, (14/15)*game.world.height, startAction,
    locFit(35, true), {fill: '#E3E3E3'});
    game.add.tween(this.startLabel).to({alpha:0},700).to({alpha:1},700).loop().start();
    
    var transition = new Transition(game, true);
    transition.start();
  },
  update: function() {
    //continually checks the update function for substate changes and collisions
    this.subState();
    game.physics.arcade.collide(this.player, this.ground, this.resetBox.reappear, null, this.resetBox);
    game.physics.arcade.overlap(this.player, this.ceiling, this.resetBox.reappear, null, this.resetBox);
    game.physics.arcade.overlap(this.player, this.resetBox, this.resetBox.losePetal, null, this.resetBox);
    game.physics.arcade.overlap(this.player, this.brambles, this.gameOver, null, this);
  },
  subState: function(){
    //breaks the play cycle into substates
    switch(this.currSubState){
      case "intro":
        //the game is paused until an action button is pressed
        this.player.animations.play('idle');
        if(this.shootInput.checkShoot()){
          this.startLabel.destroy();
          this.currSubState = "span";
        }
        break;
      case "span":
        //stops all scrolling animations, resets life petals,
        //  causes the player to travel vertically
        if(!this.player.backUpTween.isRunning){
          this.tallMountain.stopScroll();
          this.shortMountain.stopScroll();
          this.ground.stopScroll();
          this.petals.span();
          this.resetBox.span();
          this.player.span(500);
          this.brambles.killOldGroup();
          this.currSubState = "shoot";
        }
        break;
      case "shoot":
        //if an action is pressed, the player shoots towards the brambles
        if(this.resetBox.gameOver) { this.gameOver(); }
        else if(this.shootInput.checkShoot()){
          this.player.shoot();
          this.currSubState = "backUp";
        }
        break;
      case "backUp":
        //if the player finishes moving past the brambles
        //  the animations start to reset for the next step
        if(!this.player.shootTween.isRunning){
          game.global.score += 1;
          this.score.text = game.global.score;
          this.player.increaseDifficulty();
          this.brambles.increaseDifficulty();
          this.indicator.backUp(this.player);
          this.player.backUp();
          this.brambles.backUpGroup();
          this.tallMountain.backUp();
          this.shortMountain.backUp();
          this.ground.backUp();
          this.currSubState = "span";
        }
        break;
      default:
      //nothing happens
    }
  },
  gameOver: function() {
    //when the player hits the brambles or spends to much time in the
    //  span substate, game over is initiated
    this.currSubState = "";
    this.player.gameOver();
    var gameOverMenu = new GameOverMenu(game);
    game.time.events.add(2000, gameOverMenu.start, gameOverMenu);
    
  }
};