var menuState = {
  create: function() {
    game.stage.backgroundColor = '#E3E3E3';
    if(!localStorage.getItem('dandieBestScore')){
      localStorage.setItem('dandieBestScore',0);
    }
    
    this.title = new CustomText(game, game.world.centerX, (1/3)*game.world.height, "dandie", locFit(86, true), {fill: '#6B7380'});
    
    this.bestScore = new CustomText(game, game.world.centerX, game.world.centerY,
    "BEST: "+localStorage.getItem('dandieBestScore'), locFit(32, true), {fill: '#6B7380'});
    
    this.startLabel = new CustomText(game, 0, 0, 'START', 30, {fill: '#E3E3E3'});
    this.startButton = new TextButton(game, game.world.centerX, (5/6)*game.world.height,
    'startButton', this.play, this, this.startLabel);
    
    this.howToLabel = new CustomText(game, 0, 0, '?', 30, {fill: '#E3E3E3'});
    this.howToButton = new TextButton(game, (5/6)*game.world.width, (5/6)*game.world.height,
    'button', this.howTo, this, this.howToLabel);
    
    if(game.global.previousState === 'play'){
      var transition = new Transition(game, true);
      transition.start();
    }
  },
  play: function() {
    var transition = new Transition(game, false);
    transition.whiteOut.onComplete.add(function() { game.state.start('play'); });
    transition.start();
  },
  howTo: function() {
    var howToMenu = new InfoMenu(game);
    howToMenu.start();
  }
};