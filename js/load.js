var loadState = {

	preload: function () {		
		// Add a loading label 
		var loadingLabel = game.add.text(game.world.centerX, game.world.centerY, 'loading...',
		{ font: '30px '+game.global.font, fill: '#6B7380' });
		loadingLabel.anchor.setTo(0.5, 0.5);

		// Add a progress bar
		var progressBar = game.add.sprite(game.world.centerX, game.world.centerY+(game.world.centerY*0.3), 'progressBar');
		progressBar.anchor.setTo(0.5, 0.5);
		game.load.setPreloadSprite(progressBar);
		game.input.maxPointers = 1;

		// Load all assets
		game.load.spritesheet('startButton', 'assets/startButton.png',192,64);
		game.load.spritesheet('homeButton', 'assets/Home_button.png',64,64);
		game.load.spritesheet('refreshButton', 'assets/Refresh_arrow.png',64,64);
		game.load.spritesheet('button','assets/default_button.png',64,64);
		game.load.spritesheet('player','assets/player.png',44,60);
		
		game.load.image('hitBox', 'assets/hitBox.png');
		game.load.image('spore', 'assets/spore.png');
		game.load.image('ground', 'assets/ground.png');
		game.load.image('high_mountain','assets/high_mountain.png');
		game.load.image('low_mountain','assets/low_mountain.png');
		game.load.image('bramble', 'assets/bramble.png');
		game.load.image('petal', 'assets/petal.png');
		game.load.image('indicator','assets/indicator.png');
		game.load.image('menuBlock', 'assets/menu_bg.png');
	},

	create: function() { 
	  game.global.previousState = 'load';
		game.state.start('menu');
	}
};