// Initialize Phaser
var gameWidth = window.innerWidth * window.devicePixelRatio;
var gameHeight = window.innerHeight * window.devicePixelRatio;
var game = new Phaser.Game(960,540,Phaser.CANVAS,'gameDiv');

// Our 'global' variable
game.global = {
	score: 0,
	scaleRatio: (window.devicePixelRatio / 3),
	// Add other global variables
	font: "ponderosa",
	previousState: 'boot',
	upFill: '#E3E3E3',
	downFill: '#9E9E9E',
	baseW: 960,
	baseH: 540
};

// Define states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);

// Start the "boot" state
game.state.start('boot');

//Fits the number given the game dimensions
function locFit(num, dim) {
  dim = typeof dim !== 'undefined' ? dim : false;
  if(dim){ return ((num/game.global.baseH) * game.height); }
  else{ return((num/game.global.baseW) * game.width); }
}

//Scales the asset to fit the proportions of the game window
function scaleFit(asset, same, width) {
  same = typeof same !== 'undefined' ? same : true;
  width = typeof width !== 'undefined' ? width : true;
  
  var newSize;
  var newSizeW;
  var newSizeH;
  if(same) {
    if(width) {
      newSize = locFit(asset.width);
      asset.scale.setTo(newSize/asset.width,newSize/asset.width);
    }
    else {
      newSize = locFit(asset.height, true);
      asset.scale.setTo(newSize/asset.height,newSize/asset.height);
    }
  }
  else {
    newSizeW = locFit(asset.width);
    newSizeH = locFit(asset.height,true);
    asset.scale.setTo(newSizeW/asset.width,newSizeH/asset.height);
  }
}
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//The main player sprite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Dandie = function(game) {
  Phaser.Sprite.call(this,game,locFit(120),game.world.centerY,"player");
  this.anchor.setTo(0.5,0.5);
  scaleFit(this);
  game.physics.arcade.enable(this);
  this.body.setSize(this.width*0.25,this.height);
  this.body.collideWorldBounds = true;
  this.body.bounce.y = 1;
  this.spanSpeed = locFit(300, true);
  this.animations.add('idle',[0,1,2,3,4,3,2,1],6,true);
  this.shootTween = game.add.tween(this).to({x:locFit(920)},500);
  this.backUpTween = game.add.tween(this).to({x:locFit(120)},1000);
  this.spores = new Spores(game, 0, 0, 15);
  game.add.existing(this);
};
Dandie.prototype = Object.create(Phaser.Sprite.prototype);
Dandie.prototype.constructor = Dandie;
Dandie.prototype.shoot = function() {
  this.body.velocity.y = 0;
  this.shootTween.start();
};
Dandie.prototype.backUp = function() {this.backUpTween.start();};
Dandie.prototype.span = function(num) {this.body.velocity.y = this.spanSpeed;};
Dandie.prototype.increaseDifficulty = function() {
  if(game.global.score%5 !== 0) { this.spanSpeed += this.spanSpeed * 0.15; }
  else { this.spanSpeed = this.spanSpeed * 0.68; }
};
Dandie.prototype.gameOver = function(){
  this.spores.x = this.x + 10;
  this.spores.y = this.y;
  this.spores.start(true,2000,null,15);
  this.kill();
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//The emitter for player death
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Spores = function(game, x, y, max) {
  Phaser.Particles.Arcade.Emitter.call(this, game, locFit(x), locFit(y,true), max);
  game.physics.arcade.enable(this);
  this.makeParticles('spore');
  this.setXSpeed(locFit(-250), locFit(-100));
  this.gravity = -300;
};
Spores.prototype = Object.create(Phaser.Particles.Arcade.Emitter.prototype);
Spores.prototype.constructor = Spores;

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//The default button
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
RegButton = function(game, x, y, key, callBack, callBackContext) {
  Phaser.Button.call(this, game, x, y, key, callBack, callBackContext, 0, 0, 1, 0);
  this.anchor.setTo(0.5, 0.5);
  this.inputEnabled = true;
	this.input.useHandCursor = false;
	game.add.existing(this);
};
RegButton.prototype = Object.create(Phaser.Button.prototype);
RegButton.prototype.constructor = RegButton;

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//A button with text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
TextButton = function(game, x, y, key, callBack, callBackContext, text) {
  RegButton.call(this, game, x, y, key, callBack, callBackContext);
  this.label = text;
  this.addChild(this.label);
  this.events.onInputOver.add(this.labelUpOutOver, this);
  this.events.onInputOut.add(this.labelUpOutOver, this);
  this.events.onInputDown.add(this.labelDown, this);
  this.events.onInputUp.add(this.labelUpOutOver, this);
};
TextButton.prototype = Object.create(RegButton.prototype);
TextButton.prototype.constructor = TextButton;
TextButton.prototype.labelDown = function () {this.label.fill = this.game.global.downFill;};
TextButton.prototype.labelUpOutOver = function () {this.label.fill = this.game.global.upFill;};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//A custom text for the game
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
CustomText = function(game, x, y, text, size, style) {
  Phaser.Text.call(this, game, x, y, text, style);
  this.font = game.global.font;
  this.fontSize = size + "px";
  this.setShadow(2,2,'rgba(0,0,0,0.5)',0);
  this.anchor.setTo(0.5, 0.5);
  game.add.existing(this);
};
CustomText.prototype = Object.create(Phaser.Text.prototype);
CustomText.prototype.constructor = CustomText;

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//The brambles enemy group
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Brambles = function(game, gap) {
  Phaser.Group.call(this, game);
  this.enableBody = true;
  this.createMultiple(4, 'bramble');
  this.forEach(this.init, this);
  this.brambleGap = gap;
  this.start(760);
};
Brambles.prototype = Object.create(Phaser.Group.prototype);
Brambles.prototype.constructor = Brambles;
Brambles.prototype.init = function(asset) {
  asset.anchor.setTo(0.5, 0.5);
};
Brambles.prototype.start = function(num) {
  var initBranch1 = this.getFirstDead();
  initBranch1.reset(locFit(num), this.gapLocation());
  scaleFit(initBranch1, true, false);
  var initBranch2 = this.getFirstDead();
  initBranch2.reset(locFit(num),initBranch1.y - this.brambleGap - initBranch1.height);
  scaleFit(initBranch2, true, false);
  initBranch2.angle = 180;
};
Brambles.prototype.backUpGroup = function() {
  this.start(1610);
  this.forEachAlive(this.backUp, this);
};
Brambles.prototype.backUp = function(asset) {
  game.add.tween(asset).to({x:asset.x - locFit(850)}, 1000).start();
};
Brambles.prototype.killOldGroup = function() { this.forEach(this.killOld, this); };
Brambles.prototype.killOld = function(asset) { if(asset.x < 0) { asset.kill(); } };
Brambles.prototype.gapLocation = function() {
  var gap = Math.floor((Math.random() * locFit(463-this.brambleGap, true)) + locFit(this.brambleGap + 210, true) );
  return gap;
};
Brambles.prototype.increaseDifficulty = function() {
  if(game.global.score%5 === 0) {
    if((this.brambleGap - locFit(5, true)) > locFit(60)) {
      this.brambleGap -= locFit(5, true);
    }
  }
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Hitbox that controls how many times dandie can span
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
HitBox = function(game, y) {
  Phaser.Sprite.call(this, game, locFit(120), y, 'hitBox');
  this.anchor.setTo(0.5, 0.5);
  game.physics.arcade.enable(this);
  this.body.setSize(1, 1);
  this.body.immovable = true;
};
HitBox.prototype = Object.create(Phaser.Sprite.prototype);
HitBox.prototype.constructor = HitBox;

ResetBox = function(game, player, petals) {
  HitBox.call(this, game, player.y);
  this.player = player;
  this.petals = petals;
  this.spanPosition = this.y;
  this.counter = 5;
  this.gameOver = false;
};
ResetBox.prototype = Object.create(HitBox.prototype);
ResetBox.prototype.constructor = ResetBox;
ResetBox.prototype.span = function() { 
  this.spanPosition = this.player.y; 
  this.counter = 5;
  this.reset(this.x, this.spanPosition);
  this.body.setSize(1, 1);
};
ResetBox.prototype.reappear = function() {
  this.reset(this.x, this.spanPosition);
  this.body.setSize(1, 1);
};
ResetBox.prototype.losePetal = function() {
  this.counter -= 1;
  this.kill();
  if(this.counter <= 0){ this.gameOver = true; }
  else if(this.counter < 4) { this.petals.losePetal(); }
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Indicator that shows where player starts a step at
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Indicator = function(game, y) {
  Phaser.Sprite.call(this, game, 0, y, 'indicator');
  this.anchor.setTo(0, 0.5);
  scaleFit(this);
  game.add.existing(this);
};
Indicator.prototype = Object.create(Phaser.Sprite.prototype);
Indicator.prototype.constructor = Indicator;
Indicator.prototype.backUp = function(asset) { game.add.tween(this).to({y: asset.y}, 1000).start(); };

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Ground that scrolls during play
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Ground = function(game){
  Phaser.TileSprite.call(this, game, 0, game.height, 960, 77, 'ground');
  this.anchor.setTo(0, 1);
  scaleFit(this);
  game.physics.arcade.enable(this);
  this.body.immovable = true;
  this.body.setSize(this.width, this.height);
  game.add.existing(this);
};
Ground.prototype = Object.create(Phaser.TileSprite.prototype);
Ground.prototype.constructor = Ground;
Ground.prototype.backUp = function() { this.autoScroll(-850, 0); };

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Mountain that scrolls in the background
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Mountain = function(game, key, speed){
  Phaser.TileSprite.call(this, game, Math.round(game.world.centerX), Math.round(game.height*0.90), 960, 360, key);
  this.anchor.setTo(0.5, 1);
  scaleFit(this, false);
  this.scrollSpeed = locFit(speed);
  game.add.existing(this);
};
Mountain.prototype = Object.create(Phaser.TileSprite.prototype);
Mountain.prototype.constructor = Mountain;
Mountain.prototype.backUp = function() { this.autoScroll(this.scrollSpeed, 0); };

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Petal group used to show life
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Petals = function(game) {
  Phaser.Group.call(this, game);
  this.createMultiple(3, 'petal');
  this.forEach(this.init, this);
};
Petals.prototype = Object.create(Phaser.Group.prototype);
Petals.prototype.constructor = Petals;
Petals.prototype.span = function() { this.forEachDead(this.init, this, true); };
Petals.prototype.init = function(asset) {
  scaleFit(asset);
  asset.anchor.setTo(0.5, 0.5);
  asset.reset((this.getChildIndex(asset)*(-0.333) + 1)*locFit(150),this.game.height-(0.4*locFit(77, true)));
};
Petals.prototype.losePetal = function() { this.getFirstAlive().kill(); };

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Transitions to move between game stages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
Transition = function(game, intro) {
  Phaser.Graphics.call(this, game, 0, 0);
  this.beginFill(0xFFFFFF, 1);
  this.drawRect(0, 0, game.width, game.height);
  if(intro) { this.alpha = 1; }
  else { this.alpha = 0; }
	this.endFill();
	game.add.existing(this);
	this.whiteOut = game.add.tween(this).to({alpha: (this.alpha * -1) + 1}, 500);
	if(intro) { this.whiteOut.onComplete.add(this.destroy, this); }
};
Transition.prototype = Object.create(Phaser.Graphics.prototype);
Transition.prototype.constructor = Transition;
Transition.prototype.start = function() { this.whiteOut.start(); };

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Blank Modal Menu background
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
MenuBackground = function(game){
  Phaser.Graphics.call(this, game, 0, 0);
	this.beginFill(0x000000, 1);
	this.drawRect(0, 0, game.width, game.height);
	this.alpha = 0;
	this.endFill();
	this.transIn = game.add.tween(this).to({alpha: 0.3}, 500);
	this.transOut = game.add.tween(this).to({alpha: 0}, 500);
	game.add.existing(this);
};
MenuBackground.prototype = Object.create(Phaser.Graphics.prototype);
MenuBackground.prototype.constructor = MenuBackground;

MenuBlock = function(game){
  Phaser.Image.call(this, game, game.world.centerX, -400, 'menuBlock');
  scaleFit(this);
  this.anchor.setTo(0.5, 0.5);
  this.transIn = game.add.tween(this).to({y: (game.world.centerY * 0.9)}, 1000, Phaser.Easing.Bounce.Out, false, 0, 0, false);
  this.transOut = game.add.tween(this).to({y: -400}, 1000, Phaser.Easing.Bounce.In, false, 0, 0, false);
  game.add.existing(this);
};
MenuBlock.prototype = Object.create(Phaser.Image.prototype);
MenuBlock.prototype.constructor = MenuBlock;

ModalMenu = function(game) {
  this.menuBackground = new MenuBackground(game);
  this.menuBlock = new MenuBlock(game);
  this.menuBackground.transIn.chain(this.menuBlock.transIn);
  this.menuBlock.transOut.chain(this.menuBackground.transOut);
  this.menuBackground.transOut.onComplete.add(this.destroy, this);
};
ModalMenu.prototype.constructor = ModalMenu;
ModalMenu.prototype.start = function() { this.menuBackground.transIn.start(); };
ModalMenu.prototype.end = function() { 
  this.menuBlock.transOut.start();
};
ModalMenu.prototype.destroy = function() {
  this.menuBackground.destroy();
  this.menuBlock.destroy();
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Game Over Modal Menu that drops upon game over
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
GameOverMenu = function(game) {
  ModalMenu.call(this, game);
  this.homeButton = new RegButton(game, -90, 100, 'homeButton', this.changeState, this);
  this.homeButton.purpose = 'menu';
  this.restartButton = new RegButton(game, 90, 100, 'refreshButton', this.changeState, this);
  this.restartButton.purpose = 'play';
  this.gameOverText = new CustomText(game, 0, -100, 'GAME OVER', 40, {fill: '#E3E3E3'});
  this.currScoreText = new CustomText(game, 0, -40,'SCORE: '+game.global.score, 30, {fill:'#E3E3E3'});
  if(game.global.score > localStorage.getItem('dandieBestScore')) {
    localStorage.setItem('dandieBestScore', game.global.score)
  }
  this.bestScoreText = new CustomText(game, 0, -5, 'BEST: '+localStorage.getItem('dandieBestScore'), 30, {fill:'#E3E3E3'});
  this.menuBlock.addChild(this.homeButton);
  this.menuBlock.addChild(this.restartButton);
  this.menuBlock.addChild(this.gameOverText);
  this.menuBlock.addChild(this.currScoreText);
  this.menuBlock.addChild(this.bestScoreText);
};
GameOverMenu.prototype = Object.create(ModalMenu.prototype);
GameOverMenu.prototype.constructor = GameOverMenu;
GameOverMenu.prototype.changeState = function(button) {
  var transition = new Transition(game, false);
  this.menuBackground.transOut.chain(transition.whiteOut);
  transition.whiteOut.onComplete.add(function() { game.state.start(button.purpose); });
  game.global.previousState = 'play';
  this.end();
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Information Modal Menu that shows how to play the game
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
InfoMenu = function(game) {
  ModalMenu.call(this, game);
  this.menuBackground.inputEnabled = true;
  this.menuBackground.input.useHandCursor = false;
  this.menuBackground.events.onInputDown.add(this.touchExit, this, 0);
  var infoTextLabel = "-Click to shoot\n through the brambles\n\n\n-No crossing the \ntriangle > 4 times";
  this.infoText = new CustomText(game, 0, 0, infoTextLabel, 18, {fill: '#E3E3E3'});
  this.menuBlock.addChild(this.infoText);
};
InfoMenu.prototype = Object.create(ModalMenu.prototype);
InfoMenu.prototype.constructor = InfoMenu;
InfoMenu.prototype.touchExit = function(pointer) {
  var xLeft = this.menuBlock.x - ((1/2) * this.menuBlock.width );
  var xRight = this.menuBlock.x + ((1/2) * this.menuBlock.width);
  var yUp = this.menuBlock.y - ((1/2) * this.menuBlock.height);
  var yDown = this.menuBlock.y + ((1/2) * this.menuBlock.height);
  if((game.input.activePointer.x < xLeft) || (game.input.activePointer.x > xRight) || (game.input.activePointer.y < yUp) || (game.input.activePointer.y > yDown)) {
    this.menuBackground.inputEnabled = false;
    this.end();
  }
};

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Key manager used for gameplay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
ShootKey = function(game) {
  this.shootKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  this.shootClick = false;
  game.input.onDown.add(function(){this.shootClick = true;},this);
  game.input.onUp.add(function(){this.shootClick = false;},this);
};
ShootKey.prototype.constructor = ShootKey;
ShootKey.prototype.checkShoot = function() {
  if(this.shootClick || this.shootKey.isDown) {
    this.shootClick = false;
    this.shootKey.reset();
    return true;
  }
  else { return false; }
};
