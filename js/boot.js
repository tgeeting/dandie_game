var bootState = {

	preload: function () {
		game.load.image('progressBar', 'assets/progressBar.png');
	},

	create: function() { 
		// Set a background color and the physic system
		game.stage.backgroundColor = '#E3E3E3';
		game.physics.startSystem(Phaser.Physics.ARCADE);
		this.init();

		game.state.start('load');
	},
	
	init: function() {
	  game.scale.pageAlignVertically = true;
	  game.scale.pageAlignHorizontally = true;
    if(!game.device.desktop){
      game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
      game.scale.setGameSize(gameWidth, gameHeight);
    }
    else{
      game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
    }
	}
};