# **Dandie (temporary title)** #
## Author: Travis Geeting ##

An HTML 5 Game made with the Phaser Javascript Game Engine.
Playable on both desktop & mobile browsers.

Deployed on Heroku at: [dandie-game.herokuapp.com](dandie-game.herokuapp.com)

### How to Play: ###
* The player (the dandelion), will pan up and down the screen between the ground and ceiling.
* Click/Tap or press Spacebar to shoot the square in between the two green brambles.
* The player must shoot before crossing paths with the red triangle indicator (located on the right side of the screen) 4 times.
* 3 spores display each time the player may shoot.
* No spores means the game is over if the player becomes parallel with the indicator one more time
* The difficulty changes variably throughout play.

Note:
This is my first attempt at a game. There are many DRY errors which I am aware of but have yet to fix
due to lack of confidence in my use of Phaser.
As of now I have also not added mobile warning for correct orientation; it will not run correctly in
portrait mode nor will it mention so.

The index.php file is included to to wrap the game as a PHP web app so that it can be deployed via heroku.